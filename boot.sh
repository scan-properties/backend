#!/bin/sh
source venv/bin/activate
while true; do
    flask db upgrade
    if [[ "$?" == "0" ]]; then
        break
    fi

    echo Deploy command failed, retrying in 5 secs...
    sleep 5
done

exec gunicorn -b :8001 --timeout 300 -w 4 --access-logfile - --error-logfile - scan:app
