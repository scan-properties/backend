"""manatron tax

Revision ID: 9bcad2ae7e5b
Revises: 7e3024ea0808
Create Date: 2019-01-23 23:10:53.559570

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '9bcad2ae7e5b'
down_revision = '7e3024ea0808'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('property_taxes', sa.Column('amount_due', sa.Integer(), nullable=True))
    op.add_column('property_taxes', sa.Column('discount', sa.Integer(), nullable=True))
    op.add_column('property_taxes', sa.Column('interest', sa.Integer(), nullable=True))
    op.add_column('property_taxes', sa.Column('penalty', sa.Integer(), nullable=True))
    op.alter_column('users', 'access',
               existing_type=mysql.TINYINT(display_width=1),
               type_=sa.Boolean(),
               existing_nullable=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('users', 'access',
               existing_type=sa.Boolean(),
               type_=mysql.TINYINT(display_width=1),
               existing_nullable=True)
    op.drop_column('property_taxes', 'penalty')
    op.drop_column('property_taxes', 'interest')
    op.drop_column('property_taxes', 'discount')
    op.drop_column('property_taxes', 'amount_due')
    # ### end Alembic commands ###
