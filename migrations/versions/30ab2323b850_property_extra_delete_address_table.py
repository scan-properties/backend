"""property extra, delete address table

Revision ID: 30ab2323b850
Revises: 17c49a1129d1
Create Date: 2019-01-10 22:40:07.243296

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '30ab2323b850'
down_revision = '17c49a1129d1'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('property_extras',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('property_id', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=128), nullable=False),
    sa.Column('year_built', sa.Integer(), nullable=True),
    sa.Column('units', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['property_id'], ['properties.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.add_column('property_sales', sa.Column('oris_records_book', sa.String(length=32), nullable=True))
    op.add_column('property_sales', sa.Column('oris_records_page', sa.String(length=32), nullable=True))
    op.drop_column('property_sales', 'oris_book')
    op.drop_column('property_sales', 'oris_page')
    op.drop_constraint('reports_ibfk_2', 'reports', type_='foreignkey')
    op.drop_column('reports', 'address_id')
    op.alter_column('users', 'access',
               existing_type=mysql.TINYINT(display_width=1),
               type_=sa.Boolean(),
               existing_nullable=True)
    op.drop_table('addresses')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('users', 'access',
               existing_type=sa.Boolean(),
               type_=mysql.TINYINT(display_width=1),
               existing_nullable=True)
    op.add_column('reports', sa.Column('address_id', mysql.INTEGER(display_width=11), autoincrement=False, nullable=True))
    op.create_foreign_key('reports_ibfk_2', 'reports', 'addresses', ['address_id'], ['id'])
    op.add_column('property_sales', sa.Column('oris_page', mysql.VARCHAR(length=32), nullable=True))
    op.add_column('property_sales', sa.Column('oris_book', mysql.VARCHAR(length=32), nullable=True))
    op.drop_column('property_sales', 'oris_records_page')
    op.drop_column('property_sales', 'oris_records_book')
    op.create_table('addresses',
    sa.Column('id', mysql.INTEGER(display_width=11), autoincrement=True, nullable=False),
    sa.Column('value', mysql.VARCHAR(length=256), nullable=False),
    sa.Column('timestamp', mysql.DATETIME(), nullable=True),
    sa.Column('appraiser_id', mysql.VARCHAR(length=20), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    mysql_default_charset='utf8mb4',
    mysql_engine='InnoDB'
    )
    op.drop_table('property_extras')
    # ### end Alembic commands ###
