from datetime import datetime, timedelta
from flask import jsonify, request, g, current_app
from app.models import User, AuthSession
from app.auth import api
from app.auth.token import login_required
from app import db, InvalidUsage


@api.route('/new_session', methods=['POST'])
def create_session():
    body = request.get_json()
    if body.get('phone_number') is None:
        raise InvalidUsage('No params')
    
    if not User.validate_phone_number(body['phone_number']):
        raise InvalidUsage('Invalid phone number')

    user = User.query.filter_by(phone_number=body['phone_number']).first()
    if not user:
        user = User(phone_number=body['phone_number'])
        db.session.add(user)

    auth_sessions = user.auth_sessions \
        .filter(
            datetime.utcnow() - AuthSession.timestamp < 5 * 60,
            AuthSession.error is False) \
        .count()

    if auth_sessions:
        raise InvalidUsage('Wait 5 min')

    auth_session = user.new_auth_session()
    auth_session.send_message()
    if auth_session.error:
        raise InvalidUsage('Invalid phone number')

    db.session.commit()

    return jsonify(True)


@api.route('/login', methods=['POST'])
def login():
    body = request.get_json()
    phone_number = body.get('phone_number')
    auth_code = body.get('auth_code')
    if not phone_number or not auth_code:
        raise InvalidUsage('No params')

    if not User.validate_phone_number(body['phone_number']):
        raise InvalidUsage('Invalid phone number')

    user = User.query.filter_by(phone_number=phone_number).first()
    if not user:
        raise InvalidUsage('No user')
    
    if not user.verify_phone_message(auth_code):
        raise InvalidUsage('Code not valid or expired')
    
    token = user.create_token(datetime.utcnow() + timedelta(days=7))
    db.session.commit()

    result = user.to_dict()
    result['token'] = token.to_dict()
    result['token']['value'] = token.value

    return jsonify(result)


@api.route('/logout', methods=['POST'])
@login_required
def logout():
    g.token.revoke()
    db.session.commit()

    return jsonify({
        'revoked': True
    })
