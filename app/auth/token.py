from datetime import datetime
import functools
from flask import request, current_app, g
from app import db, InvalidUsage
from app.models import Token

def login_required(func):
    @functools.wraps(func)
    def decorator(*args, **kwargs):
        auth_token = request.headers.get('Auth')
        if not auth_token:
            raise InvalidUsage('Unauthorized Access', 401)

        token = Token.query.filter_by(value=auth_token).first()
        if not token:
            raise InvalidUsage('Unauthorized Access', 401)

        if token.revoked:
            raise InvalidUsage('Token is revoked', 401)

        if token.expire_in and token.expire_in < datetime.utcnow() \
                or (token.last_use
                    and datetime.utcnow() - token.last_use >= current_app.config['TOKEN_UNUSED_LIFE']):
            raise InvalidUsage('Token expired', 401)

        token.last_use = datetime.utcnow()
        db.session.commit()

        g.user = token.user
        g.token = token

        print('done')

    return decorator
