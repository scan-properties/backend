import re
import json
from base64 import b64encode
from datetime import datetime
import secrets
import requests as r
from urllib.parse import urlencode
from random import randint
from flask import current_app
from app import db, google


class Report(db.Model):
    __tablename__ = 'reports'
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    address = db.Column(db.String(128))
    a_property_id = db.Column(db.Integer, db.ForeignKey('properties.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    a_property = db.relationship(
        'Property',
        backref='report')
    user = db.relationship(
        'User',
        backref=db.backref('reports', lazy='dynamic'))

    def to_dict(self):
        return {
            'id': self.id,
            'timestamp': self.timestamp.isoformat() + 'Z',
            'address': self.address,
            'property': self.a_property.to_dict(),
            'user': self.user.to_dict() if self.user else None
        }


property_owners = db.Table(
    'property_owners',
    db.Column('property_id', db.Integer, db.ForeignKey('properties.id')),
    db.Column('owner_id', db.Integer, db.ForeignKey('owners.id'))
)


class Property(db.Model):
    __tablename__ = 'properties'
    id = db.Column(db.Integer, primary_key=True)
    use_code = db.Column(db.String(6))
    location_address = db.Column(db.String(256))
    municipality = db.Column(db.String(256))
    parcel = db.Column(db.String(17))
    subdivision = db.Column(db.String(256))
    oris_records_book = db.Column(db.String(16))
    oris_records_page = db.Column(db.String(16))
    sale_date = db.Column(db.String(32))
    legal_description = db.Column(db.String(256))
    mailing_address1 = db.Column(db.String(128))
    mailing_address2 = db.Column(db.String(128))
    map_picture = db.Column(db.LargeBinary)

    owners = db.relationship(
        'PropertyOwner', secondary=property_owners,
        backref=db.backref('properties', lazy='dynamic'),
        lazy='dynamic')
    sales = db.relationship(
        'PropertySale', backref='property', lazy='dynamic')
    extras = db.relationship(
        'PropertyExtra', backref='property', lazy='dynamic')
    taxes = db.relationship(
        'PropertyTax', backref='property', lazy='dynamic')
    buildings = db.relationship(
        'PropertyBuilding', backref='property', lazy='dynamic')
    documents = db.relationship(
        'PropertyDocument', backref='property', lazy='dynamic')
    zillow = db.relationship(
        'PropertyZillow', backref='property', uselist=False)

    def to_dict(self):
        return {
            'id': self.id,
            'use_code': self.use_code,
            'location_address': self.location_address,
            'mounicipality': self.municipality,
            'parcel_control_number': self.parcel,
            'subdivision': self.subdivision,
            'oris_records_book': self.oris_records_book,
            'oris_records_page': self.oris_records_page,
            'sale_date': self.sale_date,
            'legal_description': self.legal_description,
            'owners': [owner.to_dict() for owner in self.owners],
            'mailing_address1': self.mailing_address1,
            'mailing_address2': self.mailing_address2,
            'sales': [sale.to_dict() for sale in self.sales],
            'extras': [extra.to_dict() for extra in self.extras],
            'buildings': [building.to_dict() for building in self.buildings],
            'taxes': [tax.to_dict() for tax in self.taxes],
            'documents': [document.to_dict() for document in self.documents],
            'map_picture': self.map_picture.decode() if self.map_picture else None,
            'zillow': self.zillow.to_dict() if self.zillow else None
        }

    def set_map(self):
        req = r.post(current_app.config['PROPERTY_COORDINATES_URL'], data={
            'functionName': 'getPolyByPCN',
            'parameters': json.dumps({
                'pcn': self.parcel_control_number_short
            })
        })
        coordinates_response = json.loads(json.loads(req.text))
        points = re.findall(
            r'([\d\-\.]+[^,)]+)', coordinates_response['poly'])

        for index, coordinate in enumerate(points):
            point = coordinate.split(' ')
            points[index] = ','.join([point[1], point[0]])

        points.extend([
            'color:red',
            'weight:2'
        ])

        zoom_point = coordinates_response['point'].split(',')
        if not len(zoom_point) == 2:
            zoom_point = zoom_point.split(' ')

        zoom_point = [re.sub(r'\s', '', axis) for axis in zoom_point]
        zoom_point[0], zoom_point[1] = zoom_point[1], zoom_point[0]

        gmaps_params = {
            'center': ','.join(zoom_point),
            'zoom': 18,
            'size': '500x500',
            'maptype': 'hybrid',
            'sensor': False,
            'path': '|'.join(points),
            'key': current_app.config.get('GOOGLE_MAPS_API_KEY')
        }
        gmaps_request_url = current_app.config['GOOGLE_MAPS_URL'] + \
            '?' + urlencode(gmaps_params)
        gmaps_request_url = google.sign_url(
            input_url=gmaps_request_url,
            secret=current_app.config['GOOGLE_MAPS_API_SECRET'])

        req = r.get(gmaps_request_url)
        self.map_picture = b64encode(req.content)


class PropertyOwner(db.Model):
    __tablename__ = 'owners'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name
        }


class PropertySale(db.Model):
    __tablename__ = 'property_sales'
    id = db.Column(db.Integer, primary_key=True)
    property_id = db.Column(db.Integer, db.ForeignKey('properties.id'))
    date = db.Column(db.String(32))
    price = db.Column(db.String(32))
    oris_records_book = db.Column(db.String(32))
    oris_records_page = db.Column(db.String(32))
    sale_type = db.Column(db.String(64))
    owner = db.Column(db.String(256))

    def to_dict(self):
        return {
            'id': self.id,
            'date': self.date,
            'price': self.price,
            'oris_records_book': self.oris_records_book,
            'oris_records_page': self.oris_records_page,
            'sale_type': self.sale_type,
            'owner': self.owner
        }


class PropertyExtra(db.Model):
    __tablename__ = 'property_extras'
    id = db.Column(db.Integer, primary_key=True)
    property_id = db.Column(db.Integer, db.ForeignKey('properties.id'))
    name = db.Column(db.String(128), nullable=False)
    year_built = db.Column(db.Integer)
    units = db.Column(db.Integer)

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'year_built': self.year_built,
            'units': self.units
        }


class PropertyBuilding(db.Model):
    __tablename__ = 'property_buildings'
    id = db.Column(db.Integer, primary_key=True)
    property_id = db.Column(db.Integer, db.ForeignKey('properties.id'))
    sketch_picture = db.Column(db.LargeBinary)
    total_footage = db.Column(db.Integer)
    total_under_air = db.Column(db.Float)

    elements = db.relationship(
        'BuildingElement', backref='building', lazy='dynamic')
    footages = db.relationship(
        'BuildingFootage', backref='building', lazy='dynamic')

    def to_dict(self):
        return {
            'id': self.id,
            'sketch_picture': self.get_sketch(),
            'elements': [element.to_dict() for element in self.elements],
            'footages': [footage.to_dict() for footage in self.footages],
            'total_footage': self.total_footage,
            'total_under_air': self.total_under_air
        }

    def get_sketch(self):
        return b64encode(self.sketch_picture).decode('utf8')


class BuildingElement(db.Model):
    __tablename__ = 'building_elements'
    id = db.Column(db.Integer, primary_key=True)
    building_id = db.Column(db.Integer, db.ForeignKey('property_buildings.id'))
    name = db.Column(db.String(128), nullable=False)
    value = db.Column(db.String(128), nullable=False)

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'value': self.value
        }


class BuildingFootage(db.Model):
    __tablename__ = 'building_footages'
    id = db.Column(db.Integer, primary_key=True)
    building_id = db.Column(db.Integer, db.ForeignKey('property_buildings.id'))
    name = db.Column(db.String(128), nullable=False)
    value = db.Column(db.String(128), nullable=False)

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'value': self.value
        }


class PropertyTax(db.Model):
    __tablename__ = 'property_taxes'
    id = db.Column(db.Integer, primary_key=True)
    property_id = db.Column(db.Integer, db.ForeignKey('properties.id'))
    year = db.Column(db.Integer)
    improvement_value = db.Column(db.Integer)
    land_value = db.Column(db.Integer)
    assessed_value = db.Column(db.Integer)
    exemption_amount = db.Column(db.Integer)
    ad_valorem = db.Column(db.Integer)
    non_ad_valorem = db.Column(db.Integer)
    penalty = db.Column(db.Integer)
    interest = db.Column(db.Integer)
    discount = db.Column(db.Integer)
    amount_due = db.Column(db.Integer)

    def to_dict(self):
        return {
            'id': self.id,
            'year': self.year,
            'appraisals': {
                'improvement_value': self.improvement_value,
                'land_value': self.land_value,
                'total_market_value': self.improvement_value + self.land_value
            },
            'assessed_taxable': {
                'assessed_value': self.assessed_value,
                'exemption_amount': self.exemption_amount,
                'taxable_value': self.assessed_value - self.exemption_amount
            },
            'tax': {
                'ad_valorem': self.ad_valorem,
                'non_ad_valorem': self.non_ad_valorem,
                'total_tax': self.ad_valorem + self.non_ad_valorem
            },
            'penalty': self.penalty,
            'interest': self.interest,
            'discount': self.discount,
            'amount_due': self.amount_due
        }


class PropertyZillow(db.Model):
    __tablename__ = 'property_zillows'
    id = db.Column(db.Integer, primary_key=True)
    property_id = db.Column(db.Integer, db.ForeignKey('properties.id'))
    zid = db.Column(db.Integer)
    zestimate_amount = db.Column(db.Integer)
    zestimate_last_updated = db.Column(db.Date)
    zestimate_value_change = db.Column(db.Integer)
    zestimate_value_change_duration = db.Column(db.Integer)
    zestimate_valuation_range_low = db.Column(db.Integer)
    zestimate_valuation_range_high = db.Column(db.Integer)
    zestimate_percentile = db.Column(db.Integer)

    rent_zestimate_amount = db.Column(db.Integer)
    rent_zestimate_last_updated = db.Column(db.Date)
    rent_zestimate_value_change = db.Column(db.Integer)
    rent_zestimate_value_change_duration = db.Column(db.Integer)
    rent_zestimate_valuation_range_low = db.Column(db.Integer)
    rent_zestimate_valuation_range_high = db.Column(db.Integer)

    link_home_details = db.Column(db.String(512))
    link_home_graph = db.Column(db.String(512))
    link_home_map = db.Column(db.String(512))
    link_home_comparables = db.Column(db.String(512))
    link_region_overview = db.Column(db.String(512))
    link_region_forsale_owner = db.Column(db.String(512))
    link_region_forsale = db.Column(db.String(512))

    def to_dict(self):
        return {
            'id': self.id,
            'zid': self.zid,
            'zestimate': {
                'amount': self.zestimate_amount,
                'last_updated': self.zestimate_last_updated.strftime('%m/%d/%Y'),
                'value_change': self.zestimate_value_change,
                'valuation_range_low': self.zestimate_valuation_range_low,
                'valuation_range_high': self.zestimate_valuation_range_high,
                'percentile': self.zestimate_percentile
            },
            'rent_zestimate':{
                'amount': self.rent_zestimate_amount,
                'last_updated': self.rent_zestimate_last_updated.strftime('%m/%d/%Y'),
                'value_change': self.rent_zestimate_value_change,
                'valuation_range_low': self.rent_zestimate_valuation_range_low,
                'valuation_range_high': self.rent_zestimate_valuation_range_high
            },
            'links': {
                'home': {
                    'details': self.link_home_details,
                    'graph': self.link_home_graph,
                    'map': self.link_home_map,
                    'comparables': self.link_home_comparables,
                },
                'region': {
                    'overview': self.link_region_overview,
                    'forsale_owner': self.link_region_forsale_owner,
                    'forsale': self.link_region_forsale
                }
            }
        }


property_document_party_1 = db.Table(
    'document_party_1',
    db.Column('document_id', db.Integer, db.ForeignKey('property_documents.id')),
    db.Column('part_id', db.Integer, db.ForeignKey('document_parties.id'))
)

property_document_party_2 = db.Table(
    'document_party_2',
    db.Column('document_id', db.Integer, db.ForeignKey('property_documents.id')),
    db.Column('part_id', db.Integer, db.ForeignKey('document_parties.id'))
)


class PropertyDocument(db.Model):
    __tablename__ = 'property_documents'
    id = db.Column(db.Integer, primary_key=True)
    property_id = db.Column(db.Integer, db.ForeignKey('properties.id'))
    doc_id = db.Column(db.String(20))
    file_num = db.Column(db.String(20))
    dtype = db.Column(db.String(32))
    datetime = db.Column(db.DateTime)
    cfn = db.Column(db.String(32))
    book_type = db.Column(db.String(10))
    book = db.Column(db.Integer)
    page = db.Column(db.Integer)
    pages = db.Column(db.Integer)
    consideration = db.Column(db.Float)
    legal = db.Column(db.String(128))
    pdf = db.Column(db.LargeBinary(length=(2**32)-1))

    party_1 = db.relationship(
        'DocumentPart', secondary=property_document_party_1,
        lazy='dynamic')
    party_2 = db.relationship(
        'DocumentPart', secondary=property_document_party_2,
        lazy='dynamic')


    def to_dict(self):
        return {
            'id': self.id,
            'doc_id': int(self.doc_id),
            'file_num': int(self.file_num),
            'type': self.dtype,
            'datetime': self.datetime,
            'cfn': self.cfn,
            'book_type': self.book_type,
            'book': self.book,
            'page': self.page,
            'pages': self.pages,
            'consideration': self.consideration,
            'party_1': [part.to_dict() for part in self.party_1],
            'party_2': [part.to_dict() for part in self.party_2],
            'legal': self.legal
        }


class DocumentPart(db.Model):
    __tablename__ = 'document_parties'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name
        }


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    phone_number = db.Column(db.String(15))
    registered_at = db.Column(db.DateTime, default=datetime.utcnow)
    access = db.Column(db.Boolean, default=False)

    tokens = db.relationship('Token', backref='user', lazy='dynamic')
    auth_sessions = db.relationship('AuthSession', backref='user', lazy='dynamic')

    def __init__(self, phone_number):
        self.phone_number = phone_number.replace('+', '')

    def to_dict(self):
        return {
            'id': self.id,
            'phone_number': self.phone_number,
            'registered_at': self.registered_at.isoformat() + 'Z'
        }

    def create_token(self, expire_in=None):
        token = Token(expire_in)
        self.tokens.append(token)

        return token

    def new_auth_session(self):
        auth_session = AuthSession()
        self.auth_sessions.append(auth_session)

        return auth_session

    @staticmethod
    def validate_phone_number(phone_number):
        if re.match(r'^\+\d{11,15}$', phone_number):
            return True

        return False

    def verify_phone_message(self, auth_code):
        auth_session = self.auth_sessions \
            .filter(
                AuthSession.code == auth_code
            ) \
            .order_by(AuthSession.timestamp.desc()) \
            .first()

        if not auth_session:
            return False

        if auth_session.success is True:
            return False

        if datetime.utcnow() - auth_session.timestamp > current_app.config['AUTH_PHONE_MESSAGE_LIFE']:
            return False

        auth_session.success = True
        auth_session.success_timestamp = datetime.utcnow()

        return True


class Token(db.Model):
    __tablename__ = 'tokens'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    revoked = db.Column(db.Boolean, default=False, nullable=False)
    revoked_at = db.Column(db.DateTime)
    expire_in = db.Column(db.DateTime)
    value = db.Column(db.String(64), unique=True)
    last_use = db.Column(db.DateTime)
    auth_session_id = db.Column(db.Integer, db.ForeignKey('auth_sessions.id'))

    auth_session = db.relationship(
        'AuthSession', backref='token')

    def __init__(self, expire_in):
        self.expire_in = expire_in
        self.value = secrets.token_urlsafe(64)

    def to_dict(self):
        return {
            'id': self.id,
            'created_at': self.created_at.isoformat() + 'Z',
            'revoked': self.revoked,
            'revoked_at': self.revoked_at.isoformat() + 'Z' if self.revoked else None,
            'expire_in': self.expire_in,
            'last_use': self.last_use.isoformat() + 'Z' if self.last_use else None
        }

    def revoke(self):
        self.revoked = True
        self.revoked_at = datetime.utcnow()


class AuthSession(db.Model):
    __tablename__ = 'auth_sessions'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)
    code = db.Column(db.String(6))
    success = db.Column(db.Boolean, default=False)
    success_timestamp = db.Column(db.DateTime)
    sid = db.Column(db.String(64), default=None)
    date_sent = db.Column(db.DateTime, default=None)
    date_updated = db.Column(db.DateTime, default=None)
    status = db.Column(db.String(32), default=None)
    error = db.Column(db.Boolean, default=False)

    def __init__(self):
        self._generate_code()

    def _generate_code(self):
        code = ''
        for x in range(6):
            code += str(randint(0, 9))

        self.code = code

    def send_message(self):
        if self.sid:
            return False

        try:
            message = current_app.twilio.messages.create(
                body=f'Your verification code: {self.code}\n\nScan Properties',
                from_=current_app.config['TWILIO_PHONE_NUMBER'],
                to=self.user.phone_number
            )
        except Exception as e:
            print(e)
            self.error = True
            return

        print(message)
        self.sid = message.sid
        self.status = message.status
        self.date_sent = message.date_sent
        self.date_updated = message.date_updated
