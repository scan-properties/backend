import re
from urllib.parse import urlparse, parse_qs
from datetime import datetime, timedelta
from requests_toolbelt import MultipartEncoder
import requests as r
from bs4 import BeautifulSoup
from sentry_sdk import capture_message
from app import InvalidUsage
from app.models import PropertyDocument, DocumentPart


class Oris():
    urls = {
        'search': 'http://oris.co.palm-beach.fl.us/or_web1/new_sch.asp'
    }

    def __init__(self, *owners, last_sale=None):
        self._owners = owners
        self._last_sale = last_sale
        self.docs = []

        doc_list = set()
        for owner in owners:
            doc_list = doc_list | set(self._get_doc_list(owner))

        for doc in doc_list:
            doc.get()
            self.docs.append(doc.property_document)

    def _get_doc_list(self, owner):
        req = r.post(self.urls['search'], data={
            'search_by': 'Name',
            'search_entry': owner,
            'document_type': '',
            'FromDate': '' if not self._last_sale else f'01/01/{self._last_sale - 1}',
            'ToDate': '' if not self._last_sale else datetime.now().strftime('%d/%m/%Y'),
            'RecSetSize': 1000,
            'PageSize': 1000
        }, headers={
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) Chrome/70.0'
        })
        soup = BeautifulSoup(req.text, 'lxml')

        identification_table_element = soup.find('a', text='View')
        if not identification_table_element:
            capture_message('Owner not found in ORIS', soup)
            return []

        docs_list = []
        document_table = identification_table_element.find_parent('table')
        if not document_table:
            capture_message('Oris not found table', soup)
            return []

        rows = document_table.find_all('tr')[1:]

        for row in rows:
            cols = row.find_all('td')
            if not cols:
                continue

            if cols[4].find('font').text.strip() == 'NOT':
                continue

            doc_url = re.sub(r'\s', '', cols[0].find('a').get('href'))
            doc_url_args = parse_qs(urlparse(doc_url).query)

            doc = OrisDocument(
                doc_id=doc_url_args['doc_id'][0],
                file_num=doc_url_args['file_num'][0])
            docs_list.append(doc)

        return docs_list


class OrisDocument():
    urls = {
        'details': 'http://oris.co.palm-beach.fl.us/or_web1/details_des.asp',
        'pdf_details': 'http://oris.co.palm-beach.fl.us/or_web1/details_img.asp',
        'pdf': 'http://oris.co.palm-beach.fl.us:8080/PdfServlet/PdfServlet27'
    }

    def __init__(self, **kwargs):
        self.property_document = PropertyDocument(
            doc_id=int(kwargs.get('doc_id')),
            file_num=kwargs.get('file_num')
        )

    def __hash__(self):
        return hash((self.property_document.doc_id, self.property_document.file_num))

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.property_document.doc_id == other.property_document.doc_id \
            and self.property_document.file_num == other.property_document.file_num

    def get(self):
        self._get_details()
        self._get_pdf()

    def _get_pdf(self):
        req = r.get(self.urls['pdf_details'], params={
            'doc_id': self.property_document.doc_id,
            'pg-num': 1
        })
        req.cookies.clear()
        soup = BeautifulSoup(req.text, 'lxml')

        form = soup.find('form', {'name': 'courtform'})
        if not form:
            self.property_document.pdf = None
            return

        req_data = {}
        pdf_url = form.attrs['action']
        for input_element in form.find_all('input'):
            req_data[input_element.attrs['name']] = input_element.attrs['value']
        req_data['WaterMarkText'] = '0'

        data = MultipartEncoder(fields=req_data)
        req = r.post(pdf_url, headers={'Content-Type': data.content_type}, data=data, stream=True)
        if req.headers['Content-Type'] != 'application/pdf':
            capture_message('Did not get PDF from oris', req)
            raise InvalidUsage('Oris not working pdf')

        self.property_document.pdf = req.content

    def _get_details(self):
        req = r.get(self.urls['details'], params={
            'doc_id': self.property_document.doc_id
        })
        soup = BeautifulSoup(req.text, 'lxml')

        doc_data = soup.find_all('td', {'class': 'details_des_plain'})

        self.property_document.dtype = doc_data[0].text.strip()
        self.property_document.datetime = \
            datetime.strptime(doc_data[1].text.strip(), '%m/%d/%Y %H:%M:%S')
        self.property_document.cfn = int(doc_data[2].text.strip())
        self.property_document.book_type = doc_data[3].text.strip()
        self.property_document.book, \
            self.property_document.page = [int(num) for num in  doc_data[4].text.strip().split('/')]
        self.property_document.pages = int(doc_data[5].text.strip())
        self.property_document.consideration = float(re.sub(r'[\$,]', '', doc_data[6].text.strip()))
        self.property_document.party_1 = self._find_parties(doc_data[7].get_text())
        self.property_document.party_2 = self._find_parties(doc_data[8].get_text())
        self.property_document.legal = re.sub(r'\s{2,}', ' ', doc_data[9].text.strip())

    def _find_parties(self, text):
        result = []

        parties = re.sub(r'\s{2,}\(', ' (', text)
        for party in re.split(r'\s{3,}', parties):
            party = re.sub(r'\s{2,}', ' ', party.strip())
            if not party:
                continue

            result.append(DocumentPart(name=party))

        return result
