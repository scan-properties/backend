import re
import json
import xml.etree.ElementTree as ET
from base64 import b64encode
from datetime import datetime
import requests as r
from bs4 import BeautifulSoup
from sentry_sdk import capture_message
from flask import current_app
from app import InvalidUsage, db
from app.counties.palm_beach.oris import Oris
from app.counties.palm_beach.base import PalmBeachBase, PalmBeachElementBase
from app.models import \
    Property, \
    PropertyOwner, \
    PropertySale, \
    PropertyExtra, \
    PropertyBuilding, \
    PropertyTax, \
    BuildingElement, \
    BuildingFootage, \
    PropertyZillow


class PalmBeach(PalmBeachBase):
    def __init__(self, address=None, parcel=None, full_parcel=None):
        self._address = address
        self._parcel = parcel
        self._full_parcel = full_parcel

        if address and not parcel and not full_parcel:
            self._parcel = self.get_parcel()

    def _request_url(self, url):
        return self.urls['base'] + self.urls[url]

    @property
    def parcel(self):
        """ Property Parcel

            Returns:
            Parcel ID
        """
        return self._parcel

    @parcel.setter
    def parcel(self, value):
        self._address = value

    @property
    def address(self):
        """ Property Address

            Returns:
            Location Address
        """
        return self._address

    @address.setter
    def address(self, value):
        self._address = value

    def format_full_parcel(self):
        if len(self._parcel) != 23:
            raise ValueError('The Parcel length must be 23')

        return re.sub(r'-', '', self._parcel)

    def format_short_parcel(self):
        if len(self._parcel) != 17:
            raise ValueError('The Parcel length must be 17')

        return f'{self._parcel[0:2]}-{self._parcel[2:4]}-{self._parcel[4:6]}-' + \
            f'{self._parcel[6:8]}-{self._parcel[8:10]}-{self._parcel[10:13]}-{self._parcel[13:17]}'

    def get_parcel(self):
        """ Get parcel_id by location address.

            Returns:
            The Parcel ID.
        """

        if not self._address:
            raise ValueError('Address is not set')

        req = r.post(self._request_url('autocomplete'), json={
            'contextKey': 'RE',
            'count': 1,
            'prefixText': self.address
        })
        result = json.loads(req.text)

        if len(result['d']) > 1:
            raise InvalidUsage('Two or more addresses were found in Appraiser')

        address = result['d'][0].strip()
        if not address:
            raise InvalidUsage('Address not found in Appraiser')

        parcel = json.loads(address)['Second'].replace('P:', '')
        return parcel

    def get_tax_additional(self):
        req = r.get(self.urls['manatron'], params={
            'p': self.format_short_parcel()
        })
        soup = BeautifulSoup(req.text, 'lxml')

        tax_table = soup.find('div', {'id': '508'})
        if not tax_table:
            print(soup)
            capture_message('Address not found in Appraiser')
            raise InvalidUsage('Address not found in Manatron')

        result = []
        table_rows = tax_table.find('tbody').find_all('tr')

        for row in table_rows[:10]:
            cols = row.find_all('td')[-5:-1]
            if not cols:
                continue

            cols = [float(re.sub(r'[\$\(\),]', '', element.text)) for element in cols]
            result.append(cols)

        return result

    def scrape(self):
        req = r.get(self._request_url('details'), params={
            'parcel': self._parcel
        })
        soup = BeautifulSoup(req.text, 'lxml')

        use_code = UseCode(soup)
        municipality = Municipality(soup)
        subdivision = Subdivision(soup)
        legal_description = LegalDescription(soup)
        location_address = LocationAddress(soup)
        mailing_address = MailingAddress(soup)

        a_property = Property(
            parcel=self._parcel,
            use_code=use_code.value,
            municipality=municipality.value,
            location_address=location_address.value,
            subdivision=subdivision.value,
            legal_description=legal_description.value)
        a_property.mailing_address1, a_property.mailing_address2 = mailing_address.value

        db.session.add(a_property)

        owners_table = soup.find('td', text='Owner(s)').parent
        soup_owners = owners_table.parent.contents[1:]
        for soup_owner in soup_owners:
            owner_name = re.sub(r'( &)$', '', soup_owner.get_text())
            owner = PropertyOwner.query.filter_by(
                name=owner_name).first()

            if not owner:
                owner = PropertyOwner(name=owner_name)

            a_property.owners.append(owner)

        sales_rows = soup.find('table', {'id': 'MainContent_gvSalesInfo'}).find_all('tr')
        for row in sales_rows:
            cols = row.find_all('td')
            if not cols:
                continue

            cols = [ele.text.strip() for ele in cols]
            oris_records = re.findall(r'[\d]+', cols[2])

            owner_name = re.sub(r'( &)', '', cols[4])
            a_property.sales.append(PropertySale(
                date=cols[0],
                price=cols[1],
                oris_records_book=oris_records[0],
                oris_records_page=oris_records[1],
                sale_type=cols[3],
                owner=owner_name if owner_name else None
            ))

        manatron_tax = self.get_tax_additional()

        improvement_values = soup.find_all('span', {'id': re.compile(r'MainContent_lblImpValue')})
        land_values = soup.find_all('span', {'id': re.compile(r'MainContent_lblLandValue')})
        assessed_values = soup.find_all('span', {'id': re.compile(r'MainContent_lblAssessedValue')})
        exemption_amounts = soup.find_all('span', {'id': re.compile(r'MainContent_lblExemptionAmt')})
        ad_valorem_values = soup.find_all('span', {'id': re.compile(r'MainContent_lblAdValorem')})
        non_ad_valorem_values = soup.find_all('span', {'id': re.compile(r'MainContent_lblNonAdValorem')})

        current_year = int(soup.find('span', {'id': 'MainContent_lblTaxYr1'}).get_text())
        for (
                improvement_value, land_value,
                assessed_value, exemption_amount,
                ad_valorem, non_ad_valorem,
                tax_sale
                ) in zip(
                    improvement_values, land_values,
                    assessed_values, exemption_amounts,
                    ad_valorem_values, non_ad_valorem_values,
                    manatron_tax
                    ):
            a_property.taxes.append(PropertyTax(
                year=current_year,
                improvement_value=int(re.sub(r'\D', '', improvement_value.get_text())),
                land_value=int(re.sub(r'\D', '', land_value.get_text())),
                assessed_value=int(re.sub(r'\D', '', assessed_value.get_text())),
                exemption_amount=int(re.sub(r'\D', '', exemption_amount.get_text())),
                ad_valorem=int(re.sub(r'\D', '', ad_valorem.get_text())),
                non_ad_valorem=int(re.sub(r'\D', '', non_ad_valorem.get_text())),
                penalty=tax_sale[0],
                interest=tax_sale[1],
                discount=tax_sale[2],
                amount_due=tax_sale[3]
            ))

            current_year -= 1

        req = r.get(self._request_url('building_details'), params={
            'entity_id': self._parcel
        })
        soup = BeautifulSoup(req.text, 'lxml')

        building = PropertyBuilding()

        extra_units_rows = soup.find(
            'legend', string=re.compile('Extra')).parent.find_all('tr')
        if extra_units_rows:
            for row in extra_units_rows[1:-1]:
                cols = row.find_all('td')
                if not cols:
                    continue

                cols = [element.text.strip() for element in cols]
                a_property.extras.append(PropertyExtra(
                    name=cols[0],
                    year_built=cols[1],
                    units=cols[2]
                ))

        building_elements_table = soup.find(
            'span', {'id': 'lblBldg1'}).find_parent('table')
        footage_table = building_elements_table.find_next_sibling('table')

        building_elements_rows = building_elements_table.find_all('tr')
        if building_elements_rows:
            for row in building_elements_rows[2:]:
                cols = row.find_all('td')[1:]
                if not cols:
                    continue

                cols = [element.text.strip() for element in cols]
                building.elements.append(BuildingElement(
                    name=cols[0],
                    value=cols[1]
                ))

        footage_rows = footage_table.find_all('tr')
        if footage_rows:
            for row in footage_rows[2:-2]:
                cols = row.find_all('td')
                if not cols:
                    continue

                cols = [element.text.strip() for element in cols]
                building.footages.append(BuildingFootage(
                    name=cols[0],
                    value=cols[1]
                ))

        req = r.get(self._request_url('building_sketch'), params={
            'p_entity': self._parcel,
            'CardNo': 1
        })
        building.sketch_picture = req.content

        total_footage = []
        for row in footage_rows[-2:]:
            total_footage.append(row.find_all('td')[-1])

        building.total_footage = total_footage[0].text.strip()
        building.total_under_air = total_footage[1].text.strip()

        a_property.buildings.append(building)

        a_property.zillow = get_zillow_info(
            a_property.location_address,
            re.sub(r'\d', '', a_property.mailing_address2))

        owners_name = [owner.name for owner in a_property.owners]
        for sale in a_property.sales:
            if sale.owner:
                owners_name.append(sale.owner)
                break

        oris = Oris(
            *owners_name,
            last_sale=int(re.sub(r'\D', '', a_property.sales[0].date)) \
                if a_property.sales[0] else None)

        for doc in oris.docs:
            a_property.documents.append(doc)

        # if current_app.config['MAPS_ENABLE']:
        #     a_property.set_map()

        return a_property


def get_zillow_info(address, zipcode):
    base_url = 'http://www.zillow.com/webservice/GetSearchResults.htm'

    req = r.get(base_url, params={
        'zws-id': current_app.config['ZILLOW_API_KEY'],
        'address': address,
        'citystatezip': zipcode,
        'rentzestimate': True
    })
    xml_root = ET.fromstring(req.text)

    el_code = xml_root.find('./message/code')
    if el_code is None or int(el_code.text) != 0:
        print('Zillow: '+ xml_root.find('./message/text').text)
        return None
        # raise InvalidUsage('Address not found in Zillow')

    print(req.text)

    zillow = PropertyZillow()

    xml_result = xml_root.find('./response/results/result')
    zillow.zid = int(xml_result.find('zpid').text)
    print(xml_result.find('zpid').text)

    xml_links = xml_result.find('links')
    print(xml_links)
    zillow.link_home_details = xml_links.find('homedetails').text if xml_links.find('homedetails') is not None else None
    print(xml_links.find('homedetails'))
    zillow.link_home_graph = xml_links.find('graphsanddata').text if xml_links.find('graphsanddata') is not None else None
    zillow.link_home_map = xml_links.find('mapthishome').text if xml_links.find('mapthishome') is not None else None
    zillow.link_home_comparables = xml_links.find('comparables').text if xml_links.find('comparables') is not None else None

    xml_links = xml_result.find('./localRealEstate/region/links')
    zillow.link_region_overview = xml_links.find('overview').text if xml_links.find('overview') is not None else None
    zillow.link_region_forsale_owner = xml_links.find('forSaleByOwner').text if xml_links.find('forSaleByOwner') is not None else None
    zillow.link_region_forsale = xml_links.find('forSale').text if xml_links.find('forSale') is not None else None

    zestimate = xml_result.find('zestimate')
    zillow.zestimate_amount = int(zestimate.find('amount').text) if zestimate.find('amount') is not None else None
    zillow.zestimate_last_updated = datetime.strptime(zestimate.find('last-updated').text, '%m/%d/%Y').date() if zestimate.find('last-updated') is not None else None
    zillow.zestimate_value_change = int(zestimate.find('valueChange').text) if zestimate.find('valueChange') is not None else None
    if zillow.zestimate_value_change:
        zillow.zestimate_value_change_duration = int(zestimate.find('valueChange').attrib.get('duration')) if zestimate.find('valueChange').attrib.get('duration') is not None else None
    zillow.zestimate_valuation_range_low = int(zestimate.find('./valuationRange/low').text) if zestimate.find('./valuationRange/low') is not None else None
    zillow.zestimate_valuation_range_high = int(zestimate.find('./valuationRange/high').text) if zestimate.find('./valuationRange/high') is not None else None
    zillow.zestimate_percentile = int(zestimate.find('percentile').text) if zestimate.find('percentile') is not None else None

    rent_zestimate = xml_result.find('rentzestimate')
    if rent_zestimate is not None:
        zillow.rent_zestimate_amount = int(rent_zestimate.find('amount').text) if rent_zestimate.find('amount') is not None else None
        zillow.rent_zestimate_last_updated = datetime.strptime(rent_zestimate.find('last-updated').text, '%m/%d/%Y').date() if rent_zestimate.find('last-updated') is not None else None
        zillow.rent_zestimate_value_change = rent_zestimate.find('valueChange').text if rent_zestimate.find('valueChange') is not None else None
        if zillow.rent_zestimate_value_change:
            zillow.rent_zestimate_value_change_duration = rent_zestimate.find('valueChange').attrib.get('duration') if rent_zestimate.find('valueChange').attrib.get('duration') is not None else None
        zillow.rent_zestimate_valuation_range_low = int(rent_zestimate.find('./valuationRange/low').text) if rent_zestimate.find('./valuationRange/low') is not None else None
        zillow.rent_zestimate_valuation_range_high = int(rent_zestimate.find('./valuationRange/high').text) if rent_zestimate.find('./valuationRange/high') is not None else None

    return zillow


class UseCode(PalmBeachElementBase):
    def __init__(self, soup, required=True):
        self._soup_element = ('span', {'id': 'MainContent_lblUsecode'})
        super().__init__(soup)

    def parse(self, soup, required=True):
        value = super().parse(soup)

        use_code = re.sub(r'\D', '', value)
        if not use_code:
            self.dom_error(self._soup_element)

        if use_code not in current_app.config['APPRAISER_USE_CODE']:
            raise InvalidUsage('This type of property is not serviced', payload={
                'use_code': use_code
            })

        return use_code


class Municipality(PalmBeachElementBase):
    def __init__(self, soup, required=True):
        self._soup_element = ('span', {'id': 'MainContent_lblMunicipality'})
        super().__init__(soup)

class LocationAddress(PalmBeachElementBase):
    def __init__(self, soup, required=True):
        self._soup_element = ('span', {'id': 'MainContent_lblLocation'})
        super().__init__(soup)

class Subdivision(PalmBeachElementBase):
    def __init__(self, soup, required=True):
        self._soup_element = ('span', {'id': 'MainContent_lblSubdiv'})
        super().__init__(soup)

class LegalDescription(PalmBeachElementBase):
    def __init__(self, soup, required=True):
        self._soup_element = ('span', {'id': 'MainContent_lblLegalDesc'})
        super().__init__(soup)

class MailingAddress(PalmBeachElementBase):
    def __init__(self, soup, required=True):
        self._soup_elements = [
            ('span', {'id': 'MainContent_lblAddrLine1'}),
            ('span', {'id': 'MainContent_lblAddrLine3'})
        ]
        super().__init__(soup)

    def parse(self, soup):
        self._value = []

        for element in self._soup_elements:
            self._soup_element = element
            self._value.append(super().parse(soup))

        return self._value
