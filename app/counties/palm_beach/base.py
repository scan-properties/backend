from sentry_sdk import capture_message
from app import db, InvalidUsage


class PalmBeachBase(object):
    urls = {
        'base': 'https://www.pbcgov.org/papa',
        'autocomplete': '/AutoComplete_MasterSearch.aspx/GetResults',
        'details': '/Asps/PropertyDetail/PropertyDetail.aspx',
        'building_details': '/Asps/PropertyDetail/PrinterFriendlyStrDetail.aspx',
        'building_sketch': '/Asps/PropertyDetail/drawvector.aspx',
        'manatron': 'https://pbctax.manatron.com/tabs/propertyTax/accountdetail.aspx'
    }

    def dom_error(self, *payload):
        print(payload[0])
        capture_message('Element not found in DOM', payload)
        raise InvalidUsage('DOM Parsing error. We are working on it')


class PalmBeachElementBase(PalmBeachBase):
    def __init__(self, soup, required=True):
        self._value = None
        self._error = None

        if soup:
            self.new(soup, required)

    def __repr__(self):
        return str(self._value)

    def __str__(self):
        return str(self._value)

    def __bool__(self):
        return False if self._error else bool(self._value)

    @property
    def value(self):
        return self._value

    @property
    def error(self):
        return self._error if self._error else None

    @property
    def element(self):
        return self._soup_element

    @element.setter
    def element(self, value):
        self._soup_element = value

    def new(self, soup, required=True):
        self._value = self.parse(soup)

    def parse(self, soup, required=True):
        self._error = None

        soup_element = soup.find(*self._soup_element)
        if soup_element:
            return soup_element.get_text()

        self._error = f'Element not found in DOM'
        if required:
            self.dom_error(self._soup_element, soup)
