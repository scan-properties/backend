import json
from datetime import datetime, timedelta
from io import BytesIO
from PyPDF2 import PdfFileReader, PdfFileMerger
from flask_weasyprint import HTML, render_pdf
from flask import request, jsonify, render_template, send_file
from app import db, InvalidUsage
from app.models import Report, PropertyDocument
from app.counties import api
from app.counties.palm_beach import PalmBeach


@api.route('/reports', methods=['POST'])
def create_report():
    try:
        data = json.loads(request.data)
    except ValueError:
        raise InvalidUsage('Invalid JSON input')

    address = data.get('address')
    if not address:
        raise InvalidUsage('Invalid Usage')

    report = Report.query.filter(
        Report.address == address,
        Report.timestamp > datetime.now() - timedelta(days=1)
    ).first()

    if not report:
        report = Report(address=address)

        palm_beach = PalmBeach(address=address)
        report.a_property = palm_beach.scrape()

        db.session.add(report)
        db.session.commit()

    return jsonify({
        'report': {
            'id': report.id
        }
    })


@api.route('/reports/<int:report_id>', methods=['GET'])
def get_report(report_id):
    report = Report.query.filter_by(id=report_id).first()
    if not report:
        raise InvalidUsage('Report not found')

    pdf = PdfFileMerger()
    report_pdf = BytesIO(render_pdf(HTML(string=render_template(
        'report.html',
        a_property=report.a_property,
        report_datetime=datetime.utcnow(),
        documents=report.a_property.documents.order_by(PropertyDocument.datetime.desc())
    ))).data)
    pdf.append(PdfFileReader(report_pdf))

    for doc in report.a_property.documents.order_by(PropertyDocument.datetime.desc()):
        if doc.pdf is not None:
            pdf.append(PdfFileReader(BytesIO(doc.pdf)))

    result = BytesIO()
    pdf.write(result)
    result.seek(0)

    return send_file(
        result,
        mimetype='application/pdf',
        attachment_filename='report.pdf',
        as_attachment=True)


@api.route('/reports', methods=['GET'])
def get_report_list():
    address = request.args.get('address')
    count = request.args.get('count')
    from_id = request.args.get('from_id')

    report = Report.query
    if address:
        report = report.filter_by(address=address)
    if from_id:
        report = report.filter(Report.id < from_id)
    if count:
        report = report.limit(count)

    reports = [report.to_dict() for report in report.all()]
    return jsonify(reports)
