import os
from dotenv import load_dotenv
from datetime import timedelta


BASEDIR = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(BASEDIR, '.env'))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MAPS_ENABLE = os.environ.get('MAPS_ENABLE') in \
        ['true', True, 1, 'Yes', 'enable']
    PROPERTY_COORDINATES_URL = 'https://maps.co.palm-beach.fl.us/giswebapi/gisdata'
    GOOGLE_MAPS_URL = 'https://maps.googleapis.com/maps/api/staticmap'
    GOOGLE_MAPS_API_KEY = os.environ.get('GOOGLE_MAPS_API_KEY', None)
    GOOGLE_MAPS_API_SECRET = os.environ.get('GOOGLE_MAPS_API_SECRET', None)
    ZILLOW_API_KEY = os.environ.get('ZILLOW_API_KEY')
    SENTRY_DSN = os.environ.get('SENTRY_DSN')
    APPRAISER_USE_CODE = [
        '0100', '0101', '0104', '0105', '0110', '0130',
        '0300', '0304', '0305', '0400', '0800', '0801', '0804']
    TWILIO_CREDENTIALS = (
        os.environ.get('TWILIO_SID'),
        os.environ.get('TWILIO_TOKEN')
    )
    TWILIO_PHONE_NUMBER = os.environ.get('TWILIO_PHONE_NUMBER')
    TOKEN_UNUSED_LIFE = timedelta(days=7)
    AUTH_PHONE_MESSAGE_LIFE = timedelta(minutes=15)
