FROM python:3.6-alpine

RUN adduser -D scan-property

WORKDIR /home/scan-property

RUN apk --update --upgrade add -U --no-cache build-base linux-headers \
    ca-certificates python3 python3-dev gcc libressl-dev musl-dev \
    jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf-dev \
    ttf-ubuntu-font-family py3-lxml g++ libxslt-dev

COPY requirements.txt requirements.txt
RUN python3 -m venv venv
RUN venv/bin/pip3 install --upgrade pip
RUN venv/bin/pip3 install -r requirements.txt
RUN venv/bin/pip3 install gunicorn pymysql

COPY app app
COPY migrations migrations
COPY scan.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP scan.py

RUN chown -R scan-property:scan-property ./
USER scan-property

EXPOSE 8001
ENTRYPOINT [ "./boot.sh" ]